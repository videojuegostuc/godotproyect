extends KinematicBody2D

onready var animation : AnimatedSprite = $Animation

export var snap = false
export var move_speed = 250
export var jump_force = 5000
export var max_jump = 2000
export var gravity = 9000
export var slope_slide_treshold = 50.0
export var jump = false

var velocity = Vector2()


func _physics_process(delta):
	var move_direction = -int(Input.is_action_pressed("move_left")) + int(Input.is_action_pressed("move_right"))
	
	velocity.x = move_speed * move_direction
#	velocity.y = gravity * delta
	
	snap = is_on_floor()

	if (Input.is_action_pressed("jump") and snap):
		jump = true
		velocity.y = -max_jump
	else:
		if jump:
			if snap:
				velocity.y=0
			else:
				velocity.y+=gravity*delta
		else:
			velocity.y = gravity * delta
#	if jump and (jump_force*delta) < max_jump:
#		velocity.y = -( jump_force)+gravity * delta
#	else:
#		velocity.y = gravity * delta
#		jump = false
	update_animation(velocity)
	move_and_slide(velocity, Vector2.UP)
	
func update_animation(velocity: Vector2):
	var animation1 = "idle"
	animation.offset.y = 0
	if abs(velocity.x) > 0:
		animation.flip_h = velocity.x < 0
		animation.offset.y = 0
		animation1 = "walk"
	if velocity.y < 0:
		animation.offset.y = -67.33
		animation1 = "jump"
	if animation.animation != animation1:
		animation.play(animation1)